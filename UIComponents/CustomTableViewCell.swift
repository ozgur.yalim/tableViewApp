//
//  CustomTableViewCell.swift
//  tableViewApp
//
//  Created by Özgür Yalım on 6.06.2022.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
}
