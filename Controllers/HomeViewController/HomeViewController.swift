//
//  HomeViewController.swift
//  tableViewApp
//
//  Created by Özgür Yalım on 6.06.2022.
//

import UIKit
import Kingfisher
import Foundation

// MARK: - MyModel
struct MyModel: Codable {
    let users: [User]
}

// MARK: - User
struct User: Codable {
    let id, name, surname, email: String
    let photo: String
}


class HomeViewController: UIViewController ,UITableViewDataSource{
 
    
    @IBOutlet weak var table: UITableView!
 
    
    
    var users:MyModel=MyModel(users:[
        
        User(id: "1", name: "Özgür", surname: "Yalım", email: "Ozguryalm@gmail.com", photo: "null"),
        User(id: "2", name: "Murat", surname: "Yalım", email: "Ozguryalm@gmail.com", photo: "null"),
        User(id: "3", name: "Erdi", surname: "Yalım", email: "Ozguryalm@gmail.com", photo: "null")
    ])
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return users.users.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user=users.users[indexPath.row]
        let cell=table.dequeueReusableCell(withIdentifier: "cell",for:indexPath) as! CustomTableViewCell
        cell.label.text=user.name
        cell.imageView!.kf.setImage(with: URL(string: "https://i.stack.imgur.com/lVWFG.jpg")!,options: [.transition(ImageTransition.fade(0.8))])

      return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        table.dataSource=self
      
        // Do any additional setup after loading the view.
    }

    

}
